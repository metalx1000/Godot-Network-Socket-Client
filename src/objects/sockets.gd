extends Node2D

const HOST: String = "127.0.0.1"
const PORT: int = 4444
const RECONNECT_TIMEOUT: float = 3.0

const Client = preload("res://objects/client.gd")
var _client: Client = Client.new()

onready var label = $Label
var incoming = ""
var status = ""
var input = ""
func _process(delta):
	label.text = "Status: " + status
	label.text += "\nSent: " + input
	label.text += "\nRecieved: " + incoming
	
func _ready() -> void:
	_client.connect("connected", self, "_handle_client_connected")
	_client.connect("disconnected", self, "_handle_client_disconnected")
	_client.connect("error", self, "_handle_client_error")
	_client.connect("data", self, "_handle_client_data")
	add_child(_client)
	_client.connect_to_host(HOST, PORT)

func _connect_after_timeout(timeout: float) -> void:
	yield(get_tree().create_timer(timeout), "timeout") # Delay for timeout
	_client.connect_to_host(HOST, PORT)

func _handle_client_connected() -> void:
	status = "Client connected to server."

func _handle_client_data(data: PoolByteArray) -> void:
	
	#var message: PoolByteArray = [97, 99, 107] # Bytes for "ack" in ASCII
	incoming = data.get_string_from_utf8()
	_client.send(incoming.to_utf8())

func _handle_client_disconnected() -> void:
	status = "Client disconnected from server."
	_connect_after_timeout(RECONNECT_TIMEOUT) # Try to reconnect after 3 seconds

func _handle_client_error() -> void:
	status = "Client error."
	_connect_after_timeout(RECONNECT_TIMEOUT) # Try to reconnect after 3 seconds

func _input(event):
   # Mouse in viewport coordinates.
	if event is InputEventMouseButton:
		input = "Mouse Click/Unclick\n"
		_client.send(input.to_utf8())
	elif event is InputEventMouseMotion:
		input = event.position
		input = String(input) + "\n"
		_client.send(input.to_utf8())
	elif event.is_pressed():
		if event.as_text() == "Enter":
			input += "\n"
			_client.send(input.to_utf8())
			input = ""
		elif event.as_text() == "Space":
			input += " "
		else:
			input += event.as_text()
		
		
	
		
		
		
